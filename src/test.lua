require 'nn'
require 'image'
require 'gnuplot'
require 'csvigo'
require 'cutorch'
require 'cunn'
--require 'gnuplot'

--model = nn.Sequential()
--
--test = torch.Tensor(100,62,1)
--model:add(nn.TemporalConvolution(1,10,7))
--model:add(nn.BatchNormalization(10))
--model:add(nn.TemporalMaxPooling(2))
--model:add(nn.TemporalConvolution(10,20,7))
--model:add(nn.TemporalMaxPooling(2))
--model:add(nn.View(11*20))

--print(model:forward(test):size())


--torch.setdefaulttensortype('torch.FloatTensor')
--
--net = nn.Sequential()
--
--net:add(nn.SpatialConvolutionMM(3, 32, 3, 3, 1, 1, 1, 1))
--net:add(nn.SpatialBatchNormalization(32))
--net:add(nn.ELU())
--net:add(nn.SpatialMaxPooling(2,2,2,2))
--net:add(nn.SpatialConvolutionMM(32, 64, 3, 3, 1, 1, 1, 1))
--net:add(nn.SpatialBatchNormalization(64))
--net:add(nn.ELU())
--net:add(nn.SpatialMaxPooling(2,2,2,2))
--net:add(nn.SpatialConvolutionMM(64, 128, 3, 3, 1, 1, 1, 1))
--net:add(nn.SpatialBatchNormalization(128))
--net:add(nn.ELU())
--net:add(nn.SpatialMaxPooling(2,2,2,2))
--net:add(nn.SpatialConvolutionMM(128, 256, 3, 3, 1, 1, 1, 1))
--net:add(nn.SpatialBatchNormalization(256))
--net:add(nn.ELU())
--net:add(nn.SpatialMaxPooling(2,2,2,2))
--net:add(nn.SpatialConvolutionMM(256, 256, 3, 3, 1, 1, 1, 1))
--net:add(nn.SpatialBatchNormalization(256))
--net:add(nn.ELU())
--net:add(nn.SpatialMaxPooling(2,2,2,2))
--net:add(nn.View(12544))
--net:add(nn.Linear(12544, 4096))
--net:add(nn.BatchNormalization(4096))
--net:add(nn.ELU())
--net:add(nn.Linear(4096 , 3072))
--net:add(nn.BatchNormalization(3072))
--net:add(nn.ELU())                                                                                                                                                 
--net:add(nn.Linear(3072, 2873))
--net:add(nn.LogSoftMax())
--
--net.imageSize = 224
--net.imageCrop = 224
--
--net:evaluate()
--
--im1 = torch.rand(3, 224, 224)
--
--minibatch = torch.Tensor(2, 3, 224, 224)
--
--minibatch[1] = im1
--minibatch[2] = im1
--
--out = net:forward(minibatch)
--print(out)


--t = {}
--s = "from=world, to=Lua"
--for k, v in string.gmatch(s, "(%w+)=(%w+)") do
--  t[k] = v
--end
--
--for k,v in pairs(t) do
--	print(k,v)
--end

--x = torch.Tensor({{1,2,3},{4,5,6},{7,8,9},{10,11,12}})
--x = x:resize(4,3,1)
--y = torch.Tensor({{2,3,4},{5,6,7},{8,9,10},{11,12,13}})
--
--result = nn.AbsCriterion():forward(x,y)
--
--module = nn.Padding(1,4)
--x = module:forward(x)
--module = nn.Padding(1,-4)
--result = module:forward(x)
--print(result)
--


--x = torch.linspace(-2*math.pi,2*math.pi)
--gnuplot.plot(torch.sin(x),'~')

--test = {{1},{2},{3}}
--test = torch.Tensor(test)
--print(test:size())

--function pinv(x)
--   local u,s,v = torch.svd(x,'A')
--   s = s:narrow(1,1,torch.sum(torch.gt(s,0)))
--   s:pow(-1)
--   s = torch.diag(s)
--   s = s:cat(torch.zeros(s:size(1),u:size(1)-s:size(2)))
--   s = s:cat(torch.zeros(v:size(1)-s:size(1),s:size(2)))
--   local uu = u:t()
--   return torch.mm(v,torch.mm(s,uu))
--end
--
--x = torch.zeros(3,2)
--x[1][1] = 1
--x[2][2] = 1
--x[3][1] = math.sqrt(3)
--test = pinv(x)
--print(test)


test = torch.Tensor(1412,54,1)
--
model = nn.Sequential()
	
    model:add(nn.Padding(2,4))
    model:add(nn.Padding(2,-4))
    model:add(nn.TemporalConvolution(1,10,7))
    model:add(nn.TemporalMaxPooling(2))
    model:add(nn.TemporalConvolution(10,20,7))
    model:add(nn.TemporalMaxPooling(2))
    model:add(nn.TemporalConvolution(20,50,7))
    model:add(nn.TemporalMaxPooling(2))
    model:add(nn.View(100))
    model:add(nn.Linear(100, 144))
        print(model:forward(test):size())
    
	
	
--print(model.modules[6].weight)
--	model:add(nn.Tanh())
--	

--test = torch.Tensor({{{1,2,3},{4,5,6}},{{7,8,9},{10,11,12}}})
--print(test)
--
--model = nn.BatchNormalization(3,0,nil,false)
--
--print(model:forward(test))

--model = torch.load('bestmodel')
--model:evaluate()
--print(model.modules[6].weight)
--test = torch.load('dataset')
--X = torch.load('label')
--print(test:sub(1,1,1,144,1,1))
--print(nn.SpatialBatchNormalization(144):forward(nn.Unsqueeze(4):forward(test)):sub(1,1,1,144,1,1,1,1))
--for i = 1,11 do
--	test = model.modules[i]:forward(test)
--end

--results = model:forward(test)
--csvigo.save('E', torch.totable(results:sub(1,1)))
--csvigo.save('X', torch.totable(X:sub(1,1)))
--gnuplot.plot({'estimate', results:sub(1,1,1,10), "+"},{'testset', X:sub(1,1,1,10):squeeze(), "+"})
--print(test:sub(1,1,1,144,1,1))

--function bar()
--	return torch.Tensor(2,3):fill(5)
--end
--
--if test1 == nil then
--	test = bar()
--	
--	if test1 == nil then
--		print(test)
--	end
--end

--test = torch.Tensor(2,3):fill(5)
--print(test:type())
--test:cuda()
--print(test:type())

--onlineEstimatePlotConf = {}
--table.insert(onlineEstimatePlotConf, {title = "Plot " .. 1,
--																				drawPoints = true,
--																				strokeWidth = 0,
--																				pointSize = 1,
--																				legend = "always",
--																				labels = {"epochs", "True_Value", "Estimate"}
--																				})
--																				
--print(onlineEstimatePlotConf[1]["title"])

--
--labels_tab = {}
--for i=1,20 do
--  table.insert(labels_tab, math.ceil(i/10))
--end
--x = torch.cat(torch.Tensor(10):normal(2, 0.05), torch.Tensor(10):normal(3, 0.05), 1)
--y = torch.cat(torch.Tensor(10):normal(2, 0.05), torch.Tensor(10):normal(3, 0.05), 1)
--
--file = io.open('gaussians.txt', 'w')
--io.output(file)
--for i=1,20 do
--  io.write(string.format('%d %f %f\n', labels_tab[i], x[i], y[i]))
--end
--io.close(file)
--
----gnuplot.pngfigure('plot_labels.png')
--gnuplot.title('A Tale of Two Gaussians')
--gnuplot.raw("plot 'gaussians.txt' using 2:3:(sprintf('%d', $1)) with labels point pt 7 offset char 0.5,0.5 notitle")
--gnuplot.xlabel('x')
--gnuplot.ylabel('y')
--gnuplot.grid(true)
----gnuplot.plotflush()


--lfs = require "lfs"
--print(lfs.currentdir()) 

--print(torch.range(1,5,2))