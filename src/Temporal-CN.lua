-- To start online chart: th -ldisplay.start 8000 0.0.0.0
collectgarbage()
description = "10-20-50CN_100-144FCN_SoftPlus_1e-4_Y"

require 'nn'
require 'optim'
require 'gnuplot'
display = require 'display'
require 'csvigo'
lfs = require 'lfs'

function file_exists(file)
	f,err = io.open(file, "rb")
	if err then
		print(err)
	end
	return f ~= nil
end

function directory_exists(directory)
	attr = lfs.attributes(directory)
	if attr ~= nil and attr.mode == "directory" then
		return true
	else
		return false
	end 
end

function string.ends(String,End)
   return End=='' or string.sub(String,-string.len(End))==End
end

function lines_from(file)
	if not file_exists(file) then
		print("File not exists!")
		return {}
	end
	lines = {}
	for line in io.lines(file) do
		lines[#lines + 1] = line
	end
	return lines
end

function getA()
	local temp = lines_from(src.."dataset/A")
	local A = torch.zeros(54,144)
	for i = 1, #temp do
		if not (i == 1 or i == 2) then
			local columncounter = 1
			for value in string.gmatch(temp[i], "%S+") do
				if columncounter == 3 then
					row = value
				elseif columncounter == 4 then
					column = value
				end
				columncounter = columncounter + 1
			end
			A[row][column] = 1
		end
	end
	
	return A
end

function getX()
	local temp = lines_from(src.."dataset/X01")
	local X = torch.Tensor(2016, 144)
	for i = 1, #temp do
		local columnCounter = 1
		for value in string.gmatch(temp[i], "%S+") do
			if columnCounter%5 == 1 then
				X[i][torch.abs(columnCounter/5)+1] = value
			end
			columnCounter = columnCounter + 1
		end
	end

	return X:t()
end

function initDataset(random)
	if random==true then
		if useCuda == true then
		  local randomIndex = torch.CudaLongTensor():randperm(X:size(2))
		else
		  local randomIndex = torch.LongTensor():randperm(X:size(2))
		end
		dataset.data = Y:index(2, randomIndex):t():resize(Y:size(2),Y:size(1),1)
		dataset.label = X:index(2, randomIndex):t()
		randomIndex = nil
	else
		dataset.data = Y:t():resize(Y:size(2),Y:size(1),1)
		dataset.label = X:t()
	end
	collectgarbage()
end

function generateTrainTestSet()
	trainset.data = dataset.data:narrow(1,1,torch.ceil(dataset.data:size(1)*70/100))
	trainset.label = dataset.label:narrow(1,1,torch.ceil(dataset.data:size(1)*70/100))
	testset.data = dataset.data:narrow(1,torch.ceil(dataset.data:size(1)*70/100) + 1, dataset.data:size(1) - torch.ceil(dataset.data:size(1)*70/100))
	testset.label = dataset.label:narrow(1,torch.ceil(dataset.data:size(1)*70/100) + 1, dataset.data:size(1) - torch.ceil(dataset.data:size(1)*70/100))
end

function createModel()

	local model = nn.Sequential()

  if inputData == 'Y' then
  
  --  model for Y (54 features) as Input
  
--		model:add(nn.Squeeze())
--		model:add(nn.BatchNormalization(trainset.data:size(2)))
--		model:add(nn.Unsqueeze(3))
    model:add(nn.Padding(2,4))
    model:add(nn.Padding(2,-4))
    model:add(nn.TemporalConvolution(1,10,7))
    if activationFunction == 'tanh' then
      model:add(nn.Tanh())
    elseif activationFunction == 'relu' then
      model:add(nn.ReLU())
    elseif activationFunction == 'leakyrelu' then
      model:add(nn.LeakyReLU())
    elseif activationFunction == 'softplus' then
      model:add(nn.SoftPlus())
    end
    model:add(nn.TemporalMaxPooling(2))
    model:add(nn.TemporalConvolution(10,20,7))
    if activationFunction == 'tanh' then
      model:add(nn.Tanh())
    elseif activationFunction == 'relu' then
      model:add(nn.ReLU())
    elseif activationFunction == 'leakyrelu' then
      model:add(nn.LeakyReLU())
    elseif activationFunction == 'softplus' then
      model:add(nn.SoftPlus())
    end
    model:add(nn.TemporalMaxPooling(2))
    model:add(nn.TemporalConvolution(20,50,7))
    if activationFunction == 'tanh' then
      model:add(nn.Tanh())
    elseif activationFunction == 'relu' then
      model:add(nn.ReLU())
    elseif activationFunction == 'leakyrelu' then
      model:add(nn.LeakyReLU())
    elseif activationFunction == 'softplus' then
      model:add(nn.SoftPlus())
    end
    model:add(nn.TemporalMaxPooling(2))
 
    model:add(nn.View(100))
    model:add(nn.Linear(100,144))
    if activationFunction == 'tanh' then
      model:add(nn.Tanh())
    elseif activationFunction == 'relu' then
      model:add(nn.ReLU())
    elseif activationFunction == 'leakyrelu' then
      model:add(nn.LeakyReLU())
    elseif activationFunction == 'softplus' then
      model:add(nn.SoftPlus())
    end
  
  elseif inputData == 'A^+Y' then 
	
	-- model for A^+Y (144 features) as input

--	model:add(nn.Squeeze())
--	model:add(nn.BatchNormalization(trainset.data:size(2)))
--	model:add(nn.Unsqueeze(3))
--	model:add(nn.Unsqueeze(4))
--	model:add(nn.SpatialBatchNormalization(144))
--	model:add(nn.Squeeze(4))
  	model:add(nn.Padding(2,4))
  	model:add(nn.Padding(2,-4))
  	model:add(nn.TemporalConvolution(1,10,7))
  	if activationFunction == 'tanh' then
  		model:add(nn.Tanh())
  	elseif activationFunction == 'relu' then
  		model:add(nn.ReLU())
  	elseif activationFunction == 'leakyrelu' then
  		model:add(nn.LeakyReLU())
  	elseif activationFunction == 'softplus' then
  		model:add(nn.SoftPlus())
  	end
  	model:add(nn.TemporalMaxPooling(2))
  	model:add(nn.TemporalConvolution(10,20,7))
  	if activationFunction == 'tanh' then
  		model:add(nn.Tanh())
  	elseif activationFunction == 'relu' then
  		model:add(nn.ReLU())
  	elseif activationFunction == 'leakyrelu' then
  		model:add(nn.LeakyReLU())
  	elseif activationFunction == 'softplus' then
  		model:add(nn.SoftPlus())
  	end
  	model:add(nn.TemporalMaxPooling(2))
   	model:add(nn.TemporalConvolution(20,50,7))
  	if activationFunction == 'tanh' then
  		model:add(nn.Tanh())
  	elseif activationFunction == 'relu' then
  		model:add(nn.ReLU())
  	elseif activationFunction == 'leakyrelu' then
  		model:add(nn.LeakyReLU())
  	elseif activationFunction == 'softplus' then
  		model:add(nn.SoftPlus())
  	end
  	model:add(nn.TemporalMaxPooling(2))
 
  	model:add(nn.View(650))
    model:add(nn.Linear(650, 300))
    if activationFunction == 'tanh' then
      model:add(nn.Tanh())
    elseif activationFunction == 'relu' then
      model:add(nn.ReLU())
    elseif activationFunction == 'leakyrelu' then
      model:add(nn.LeakyReLU())
    elseif activationFunction == 'softplus' then
      model:add(nn.SoftPlus())
    end
    model:add(nn.Linear(300, 144))
    if activationFunction == 'tanh' then
      model:add(nn.Tanh())
    elseif activationFunction == 'relu' then
      model:add(nn.ReLU())
    elseif activationFunction == 'leakyrelu' then
      model:add(nn.LeakyReLU())
    elseif activationFunction == 'softplus' then
      model:add(nn.SoftPlus())
    end
--    model:add(nn.Linear(500, 144))
--    if activationFunction == 'tanh' then
--      model:add(nn.Tanh())
--    elseif activationFunction == 'relu' then
--      model:add(nn.ReLU())
--    elseif activationFunction == 'leakyrelu' then
--      model:add(nn.LeakyReLU())
--    elseif activationFunction == 'softplus' then
--      model:add(nn.SoftPlus())
--    end
    
--  	model:add(nn.Linear(33*20, 300))
--  	if activationFunction == 'tanh' then
--  		model:add(nn.Tanh())
--  	elseif activationFunction == 'relu' then
--  		model:add(nn.ReLU())
--  	elseif activationFunction == 'leakyrelu' then
--  		model:add(nn.LeakyReLU())
--  	elseif activationFunction == 'softplus' then
--  		model:add(nn.SoftPlus())
--  	end
--  	model:add(nn.Linear(300, 144))
--  	if activationFunction == 'tanh' then
--  		model:add(nn.Tanh())
--  	elseif activationFunction == 'relu' then
--  		model:add(nn.ReLU())
--  	elseif activationFunction == 'leakyrelu' then
--  		model:add(nn.LeakyReLU())
--  	elseif activationFunction == 'softplus' then
--  		model:add(nn.SoftPlus())
--  	end

  end

	local criterion = nn.SmoothL1Criterion()
	--local criterion = nn.AbsCriterion()
	--local criterion = nn.MSECriterion()
	
	if useCuda == true then
	 model:cuda()
	 criterion:cuda()
	end

	return model, criterion
end

function train(data)
	local params, grads = model:getParameters()

	local feval = function(x)
		if x ~= params then
			params:copy(x)
		end
		grads:zero()

		-- forward
		local outputs = model:forward(data.data)
		local loss = criterion:forward(outputs, data.label)
		-- backward
		local dloss_doutput = criterion:backward(outputs, data.label)
		model:backward(data.data, dloss_doutput)

		return loss, grads
	end

	local optim_state = {learningRate = learningRate, learningRateDecay = 1e-5}
	--local optim_state = {learningRate = learningRate}

	local _, loss = optim.adagrad(feval, params, optim_state)
--	for k,v in pairs(loss) do
--		print(k,v)
--	end
	return loss[1]
end

function test()
	local outputs = model:forward(testset.data)
	local loss = criterion:forward(outputs, testset.label)
	return outputs, loss
end

function pinv(x)
	local u,s,v = torch.svd(x,'A')
	s = s:narrow(1,1,torch.sum(torch.gt(s,0)))
	s:pow(-1)
	s = torch.diag(s)
	if u:size(1)-s:size(2) > 0 then
		s = s:cat(torch.zeros(s:size(1),u:size(1)-s:size(2)),2)
	end
	if v:size(1)-s:size(1) > 0 then
		s = s:cat(torch.zeros(v:size(1)-s:size(1),s:size(2)),1)
	end
	local uu = u:t()
	return torch.mm(v,torch.mm(s,uu))
end

function saveModel(model)
	local tempModel = model:clone()
	tempModel:clearState()
	torch.save(ResultsDir.."bestmodel", tempModel)
end

if string.ends(lfs.currentdir(), 'src') then
	src = ""
else
	src = "src/"
end

ResultsDir = src.."Results/"..description

if not directory_exists(src.."Results") then
	lfs.mkdir(src.."Results")
end

directory_counter = 1
tempResultsDir = ResultsDir
while directory_exists(tempResultsDir) do
  tempResultsDir = ResultsDir.."("..directory_counter..")"
  directory_counter = directory_counter + 1
end
ResultsDir = tempResultsDir.."/"
print("Results Directory set to: "..ResultsDir.."\n")
lfs.mkdir(ResultsDir)

cmd = torch.CmdLine()
cmd:option('-cuda', 1, 'Set to 1 if you want to use Cuda')
cmd:text()
opt = cmd:parse(arg)

useCuda = true
if opt.cuda == 0 then
  useCuda = false
end

if useCuda == true then
  require 'cunn'
  require 'cutorch'
end

-- 'Y', 'A^+Y'
inputData = 'Y'

-- tanh, relu, leakyrelu, softplus
activationFunction = 'softplus'

print("Loading A Matrix...")
A = getA()
print("A Matrix loaded.")
print("Loading X Matrix...")
X = getX()
print("X Matrix loaded.")
print("Computing Y Matrix...")
Y = (A*X)
if inputData == 'A^+Y' then
  Y = pinv(A)*Y
end
print("Y Matrix computed.")

if useCuda == true then
  A = A:cuda()
  X = X:cuda()
  Y = Y:cuda()
end

dataset = {}
trainset = {}
testset = {}

print("Initializing dataset...")
initDataset(false)
print("Dataset Initialized.")
print("Generating Train/Test sets...")
generateTrainTestSet()
print("Train/Test sets generated.")

print("Creating model...")
model, criterion = createModel()
params, grads = model:getParameters()
params:uniform(-0.1, 0.1)
model:clearState()
print("Model created.")

BATCHSIZE = trainset.data:size(1)
--BATCHSIZE = 300
nIterations = 1000
learningRate = 1e-4

print("---TRAIN---")
internalLosses = {}
externalLosses = {}
onlineErrorPlot = {}
onlineErrorPlotCounter = 1
if file_exists('.onlineErrorPlotConf') then
	onlineErrorPlotConf = torch.load('.onlineErrorPlotConf')
else
	onlineErrorPlotConf = {}
end
onlineErrorPlotConf.connectSeparatedPoints = true
onlineErrorPlotConf.labels = {"epochs", "Internal Error", "External Error"}
onlineErrorPlotConf.legend = "always"

if file_exists('.onlineSREPlotConf') then
	onlineSREPlotConf = torch.load('.onlineSREPlotConf')
else
	onlineSREPlotConf = {}
end
onlineSREPlotConf.connectSeparatedPoints = true
onlineSREPlotConf.labels = {"ODs", "SRE"}
onlineSREPlotConf.legend = "always"

if file_exists('.onlineTREPlotConf') then
	onlineTREPlotConf = torch.load('.onlineTREPlotConf')
else
	onlineTREPlotConf = {}
end
onlineTREPlotConf.connectSeparatedPoints = true
onlineTREPlotConf.labels = {"Time", "TRE"}
onlineTREPlotConf.legend = "always"

--onlineErrorPlotConf = {--drawPoints=true,
--	connectSeparatedPoints=true,
--	labels = {"epochs", "Internal Error", "External Error"},
--	legend = "always"
--}
--onlineEstimatePlotConf1 = {drawPoints = true,
--													strokeWidth = 0,
--													pointSize = 1,
--													legend = "always",
--													labels = {"epochs", "True_Value", "Estimate"}
--}
--onlineEstimatePlotConf2 = {drawPoints = true,
--													strokeWidth = 0,
--													pointSize = 1,
--													legend = "always",
--													labels = {"epochs", "True_Value", "Estimate"},
--}
if file_exists('.onlineEstimatePlotConf') then
	onlineEstimatePlotConf = torch.load('.onlineEstimatePlotConf')
else
	onlineEstimatePlotConf = {}
end
for i = 1, 10 do
	if onlineEstimatePlotConf[i] == nil then
		onlineEstimatePlotConf[i] = {}
	end
	onlineEstimatePlotConf[i].title = "Plot " .. i
	onlineEstimatePlotConf[i].drawPoints = true
	onlineEstimatePlotConf[i].strokeWidth = 0
	onlineEstimatePlotConf[i].pointSize = 1
	onlineEstimatePlotConf[i].legend = "always"
	onlineEstimatePlotConf[i].labels = {"epochs", "True_Value", "Estimate"}
--	table.insert(onlineEstimatePlotConf, {title = "Plot " .. i,
--																				drawPoints = true,
--																				strokeWidth = 0,
--																				pointSize = 1,
--																				legend = "always",
--																				labels = {"epochs", "True_Value", "Estimate"}
--																				})
end
--onlineImageConf = {width=300}

for i = 1, nIterations do
	internalLosses[i] = {}
	model:training()
	for k = 1, torch.ceil(trainset.data:size(1)/BATCHSIZE) do
		local tempDataset = {}
		tempDataset.data = trainset.data:sub((k-1)*BATCHSIZE+1,math.min(k*BATCHSIZE,trainset.data:size(1)))
		tempDataset.label = trainset.label:sub((k-1)*BATCHSIZE+1,math.min(k*BATCHSIZE,trainset.data:size(1)))
		internalLosses[i][k] = train(tempDataset)
		print("Iteration "..i..", Batch "..k..", Internal Loss: "..internalLosses[i][k])
		table.insert(onlineErrorPlot, {onlineErrorPlotCounter, internalLosses[i][k], null})
		onlineErrorPlotConf.win = display.plot(onlineErrorPlot, onlineErrorPlotConf)
		torch.save(ResultsDir..".onlineErrorPlotConf", onlineErrorPlotConf)
		onlineErrorPlotCounter = onlineErrorPlotCounter + 1
	end
	model:evaluate()
	estimations,externalLosses[i] = test()
	
	if minExternalLoss == nil or minExternalLoss > externalLosses[i] then
		bestEstimate = estimations
		saveModel(model)
		csvigo.save(ResultsDir..'estimation.csv', torch.totable(estimations))
		csvigo.save(ResultsDir..'label.csv', torch.totable(testset.label))
		
		SRE = torch.cdiv(torch.norm(estimations - testset.label,2,1),torch.norm(testset.label,2,1)):t()
		TRE = torch.cdiv(torch.norm(estimations - testset.label,2,2),torch.norm(testset.label,2,2))
		csvigo.save(ResultsDir..'SRE.csv', torch.totable(SRE))
		csvigo.save(ResultsDir..'TRE.csv', torch.totable(TRE))
		
		onlineEstimatePlot = {}
		for plot = 1, 10 do
			for plotDataCounter = 1, testset.data:size(1) do
				onlineEstimatePlot[plotDataCounter] = {plotDataCounter, testset.label[plotDataCounter][plot], bestEstimate[plotDataCounter][plot]}
			end
			onlineEstimatePlotConf[plot].win = display.plot(onlineEstimatePlot, onlineEstimatePlotConf[plot])
		end
		torch.save(ResultsDir..".onlineEstimatePlotConf", onlineEstimatePlotConf)
		
		if useCuda == true then		
			onlineSREPlotConf.win = display.plot(torch.cat(torch.range(0,SRE:size(1)-1):cuda(),SRE,2), onlineSREPlotConf)
			onlineTREPlotConf.win = display.plot(torch.cat(torch.range(0,TRE:size(1)-1):cuda(),TRE,2), onlineTREPlotConf)
		else
			onlineSREPlotConf.win = display.plot(torch.cat(torch.range(0,SRE:size(1)-1),SRE,2), onlineSREPlotConf)
			onlineTREPlotConf.win = display.plot(torch.cat(torch.range(0,TRE:size(1)-1),TRE,2), onlineTREPlotConf)
		end
		torch.save(ResultsDir..".onlineSREPlotConf", onlineSREPlotConf)
		torch.save(ResultsDir..".onlineTREPlotConf", onlineTREPlotConf)

		gnuplot.pngfigure(ResultsDir.."SRE.png")
		gnuplot.xlabel("ODs")
		gnuplot.ylabel("SRE")
		gnuplot.title("SRE - "..description)
		gnuplot.plot({'SRE', torch.squeeze(SRE), "-"})
		gnuplot.plotflush()
		
		gnuplot.pngfigure(ResultsDir.."TRE.png")
		gnuplot.xlabel("Time")
		gnuplot.ylabel("TRE")
		gnuplot.title("TRE - "..description)
		gnuplot.plot({'TRE', torch.squeeze(TRE), "-"})
		gnuplot.plotflush()
		
		gnuplot.closeall()
		
		for plot = 1, bestEstimate:size(2) do
			gnuplot.pngfigure(ResultsDir.."Plot"..plot..".png")
			gnuplot.raw("set pointsize 0.25")
			gnuplot.title("Plot "..plot.." - "..description)
			gnuplot.xlabel("Time")
			gnuplot.ylabel("kbps")
			gnuplot.plot({'estimate', bestEstimate:select(2,plot), "+-"},{'true', testset.label:squeeze():select(2,plot), "+-"})
			gnuplot.plotflush()
		end
		gnuplot.closeall()

		minExternalLoss = externalLosses[i]

	end
	print("-----External Loss: "..externalLosses[i].."\n")
	onlineErrorPlot[onlineErrorPlotCounter-1][3] = externalLosses[i]
	onlineErrorPlotConf.win = display.plot(onlineErrorPlot, onlineErrorPlotConf)

  gnuplot.pngfigure(ResultsDir.."Loss.png")
  gnuplot.xlabel("Batch-Iteration")
  gnuplot.ylabel("Loss")
  gnuplot.title("Loss - "..description)
  gnuplot.raw("set pointsize 0.25")
  gnuplot.plot({'internal_loss',torch.range(1,torch.Tensor(internalLosses):storage():size()), torch.Tensor(internalLosses):view(torch.Tensor(internalLosses):storage():size()),'-'},
               {'external_loss',torch.range(1,torch.Tensor(internalLosses):storage():size(), torch.ceil(trainset.data:size(1)/BATCHSIZE)), torch.Tensor(externalLosses),'-'})
  gnuplot.closeall()


	collectgarbage()
end

--gnuplot.plot({'estimate', bestEstimate:select(2,1), "+"},{'testset', torch.Tensor(testset.label):squeeze():select(2,1), "+"})
--gnuplot.plot({'estimate', estimations:select(2,1), "+"},{'testset', torch.Tensor(testset.label):squeeze():select(2,1), "+"})

-- Write estimations and testset to file 
